package com.scotiabank.myapp;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AccountsService {

	record CustAccount(String userId, BigDecimal value) { }

	public void getCustomerBalance(List<CustAccount> accounts) {
		Map<String, CustAccount> sumAccounts = new HashMap<>();

		for (CustAccount acc : accounts) {
			if (!sumAccounts.containsKey(acc.userId())) {
				sumAccounts.put(acc.userId(), acc);
				continue;
			}
			var acc2 = sumAccounts.get(acc.userId());
			var newAcc = new CustAccount(acc.userId(), acc2.value().add(acc.value()));
			sumAccounts.put(acc.userId(), newAcc);
		}

		for (Map.Entry<String, CustAccount> acc : sumAccounts.entrySet()) {
			System.out.println(acc.getValue());
		}

	}

	/*
	 * 

	List<CustAccount> accounts = Arrays.asList(
	   new CustAccount("1", BigDecimal.valueOf(50)),
	   new CustAccount("1", BigDecimal.valueOf(30)),
	   new CustAccount("2", BigDecimal.valueOf(70)),
	   new CustAccount("4", BigDecimal.valueOf(25))
	);
	
	getCustomerBalance(accounts);

	 */
	
}
